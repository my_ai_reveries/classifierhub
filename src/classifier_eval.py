import torch
from torchvision import datasets, transforms
from sklearn.metrics import classification_report
import numpy as np
import matplotlib.pyplot as plt
import os
from tqdm import tqdm
from os.path import isfile, join
import re
from PIL import Image
import yaml

def load_max_epoch_model(path_to_models: str):

    model_names = [f for f in os.listdir(path_to_models) if isfile(join(path_to_models, f))]

    epoch_max = 0

    pattern = re.compile(r'model_epoch_(\d+)\.pth')

    for name in model_names :
        match = pattern.match(name)
        if (match) :
            tmp = int(match.group(1))   
            if(tmp > epoch_max ) :
                epoch_max = tmp

    print(f'MAX EPOCH: {epoch_max}')

    return  torch.load(f"{path_to_models}/model_epoch_{epoch_max}.pth", map_location=torch.device('cpu'))




def predict(model,  idx_to_class, transform,  img_path):
    '''
    Function to predict the class of a single test image
    Parameters
        :param model: Model to test
        :param test_image_path: Test image

    '''


    transform = transform['test']

    test_image = Image.open(img_path)
    test_image_tensor = transform(test_image)

    if torch.cuda.is_available():
        test_image_tensor = test_image_tensor.view(1, 3, 480, 480).cuda()
    else:
        test_image_tensor = test_image_tensor.view(1, 3, 480, 480)

    with torch.no_grad():
        model.eval()
        # Model outputs log probabilities
        out = model(test_image_tensor)
        ps = torch.exp(out)
 
        topk, topclass = ps.topk(3, dim=1)
        pred = idx_to_class[topclass.cpu().numpy()[0][0]]
        score = topk.cpu().numpy()[0][0]

    return pred, score


def testset_testing(model, idx_to_class, img_transforms, path_to_data):
    images = dict() # collect path and correspondent true class
    y_pred = []
    mislist = []

    print('\n--- Testing ---')

    # load true class
    for _dir in os.listdir(f'{path_to_data}/test'):
        path = f'{path_to_data}/test/{_dir}'
        temp = {k: _dir for k in list(map(lambda x: os.path.join(path, x), os.listdir(path)))}
        images.update(temp)
        print(f'{_dir}:\t{len(temp.keys())}')

    # predict and compare with true class
    for test_image_path, true_class in tqdm(images.items()):
        pred_class, score = predict(model, idx_to_class, img_transforms, test_image_path)

        y_pred.append(pred_class)

        if pred_class != true_class:
            name = test_image_path.split('/')[-1].strip('.jpg')

            mislist.append(name)

            test_image = Image.open(test_image_path)

            plt.figure()
            plt.imshow(test_image)
            plt.title(f'{name}   True: {true_class}   Pred: {pred_class}   Score:{score}')

    return np.array(list(images.values())), np.array(y_pred), mislist

def run_eval(): 
    
    # settings:
    with open("./configs.yml", encoding="utf-8") as file:
        configs = yaml.load(file, Loader=yaml.FullLoader)

    PATH_TO_MODELS = configs["test"]["path_to_models"]
    PATH_TO_DATA = configs["test"]["path_to_data"]
    
    
    # setup:
    
    # Load Data and class settings
    # ----------------------------
    
    # transforms
    img_transforms = {

        'test': transforms.Compose([
            transforms.Resize(size=(480, 480)),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406],
                                [0.229, 0.224, 0.225])
        ])
    }

    # Set train and valid directory paths
    valid_directory = os.path.join(PATH_TO_DATA, 'val')
    test_directory = os.path.join(PATH_TO_DATA, 'test')

    # Number of classes
    num_classes = len(os.listdir(valid_directory))  #10#2#257
    print(f'Number of Classes: {num_classes}')

    # Load Data from folders
    data = {
        'val': datasets.ImageFolder(root=test_directory, transform=img_transforms['test'])
    }
    
    # Get a mapping of the indices to the class names, in order to see the output classes of the test images.
    idx_to_class = {v: k for k, v in data['val'].class_to_idx.items()}
   

  

    
    trained_model = load_max_epoch_model(PATH_TO_MODELS)
    
    print('Device: cpu')
    print(f'Number of Classes: {len(idx_to_class)}')
    print({f'Class mapping: {idx_to_class}'})
    
    # evaluation
    
    y_true, y_predicted, misslist = testset_testing(trained_model, idx_to_class, img_transforms, PATH_TO_DATA)

    print(classification_report(y_true, y_predicted))
    
    # TODO return metrics / plot

