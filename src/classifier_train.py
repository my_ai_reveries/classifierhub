#%%
import torch
import torchvision
from torchvision import datasets, models, transforms
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader
import time
# from torchsummary import summary

import numpy as np
import matplotlib.pyplot as plt
import os

from PIL import Image

#%%
# Settings
# --------

# Iterate Version while experimenting
NUM_EPOCHS = 20
BATCH_SIZE = 8

# Paths for Azure DWS Training
PATH_TO_MODELS = './model/output'

if(not os.path.exists(PATH_TO_MODELS)) :
    os.makedirs(PATH_TO_MODELS)
    print("created train folder")

PATH_TO_DATA = './data/'

# ==============================
#          Preparation
# ==============================


# Transforms
# ----------

image_transforms = {
    'train': transforms.Compose([
        transforms.Resize(size=(480, 480)),
        transforms.RandomRotation(degrees=15),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406],
                             [0.229, 0.224, 0.225])
    ]),
    'valid': transforms.Compose([
        transforms.Resize(size=(480, 480)),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406],
                             [0.229, 0.224, 0.225])
    ]),
    'test': transforms.Compose([
        transforms.Resize(size=(480, 480)),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406],
                             [0.229, 0.224, 0.225])
    ])
}


# Load Data
# ---------

# Set train and valid directory paths
train_directory = os.path.join(PATH_TO_DATA, 'train')
valid_directory = os.path.join(PATH_TO_DATA, 'val')
test_directory = os.path.join(PATH_TO_DATA, 'test')

# Number of classes
num_classes = len(os.listdir(valid_directory))  #10#2#257
print(f'Number of Classes: {num_classes}')

# Load Data from folders
data = {

    'train': datasets.ImageFolder(root=train_directory, transform=image_transforms['train']),
    'valid': datasets.ImageFolder(root=valid_directory, transform=image_transforms['valid']),
    'test': datasets.ImageFolder(root=test_directory, transform=image_transforms['test'])
}

# Get a mapping of the indices to the class names, in order to see the output classes of the test images.
idx_to_class = {v: k for k, v in data['train'].class_to_idx.items()}
print(f'Class mapping:\n{idx_to_class}')

# Size of Data, to be used for calculating Average Loss and Accuracy
train_data_size = len(data['train'])
valid_data_size = len(data['valid'])
test_data_size = len(data['test'])

# Create iterators for the Data loaded using DataLoader module
train_data_loader = DataLoader(data['train'], batch_size=BATCH_SIZE, shuffle=True)
valid_data_loader = DataLoader(data['valid'], batch_size=BATCH_SIZE, shuffle=True)
test_data_loader = DataLoader(data['test'], batch_size=BATCH_SIZE, shuffle=True)


# Load Data
# ---------

# Select device
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# # Load pretrained Convnext Model from local file
 
PATH_TO_CONVNEXT ='./models/convnext_base-6075fbad.pth'


convnxt = torchvision.models.convnext_base()
convnxt.load_state_dict(torch.load(PATH_TO_CONVNEXT)) # Also available as ConvNeXt_Large_Weights.DEFAULT or base or tiny ...; You can also use strings, e.g. weights='DEFAULT' or weights='IMAGENET1K_V1'. However, we are not able to download it directly with the model.





#Freezing the base model and changing the output layer to suit our needs
n_inputs = None
for name, child in convnxt.named_children():
    if name == 'classifier':
        for param in child.parameters():
            param.requires_grad = True
        for sub_name, sub_child in child.named_children():
            if sub_name == '2':
                n_inputs = sub_child.in_features
    else:
        for param in child.parameters():
            param.requires_grad = False
        


#Classifier: Turns the feature vector into a vector with the same dimensionality as the number of required output classes 
seq =nn.Sequential(
    torchvision.models.convnext.LayerNorm2d((n_inputs,), eps=1e-07, elementwise_affine=True),
    nn.Flatten(start_dim=1, end_dim=-1),
    nn.Linear(n_inputs, 256, bias=True),
    nn.ReLU(),
    nn.Dropout(0.4),
    nn.Linear(256, num_classes),
    nn.LogSoftmax(dim=1)
)


convnxt.classifier = seq#sequential_layers


# Convert model to be used on GPU
convnxt = convnxt.to(device)


# Define Optimizer and Loss Function
loss_func = nn.NLLLoss()
optimizer = optim.Adam(convnxt.parameters(),  lr= 1e-4)


# ==============================
#           Training
# ==============================

def train_and_validate(model, loss_criterion, optimizer, epochs=25):
    '''
    Function to train and validate
    Parameters
        :param model: Model to train and validate
        :param loss_criterion: Loss Criterion to minimize
        :param optimizer: Optimizer for computing gradients
        :param epochs: Number of epochs (default=25)

    Returns
        model: Trained Model with best validation accuracy
        history: (dict object): Having training loss, accuracy and validation loss, accuracy
    '''

    history = []
    best_loss = 100000.0
    best_epoch = None

    for epoch in range(epochs):
        epoch_start = time.time()

        # Set to training mode
        model.train()
        print('start')
        # Loss and Accuracy within the epoch
        train_loss = 0.0
        train_acc = 0.0

        valid_loss = 0.0
        valid_acc = 0.0

        for i, (inputs, labels) in enumerate(train_data_loader):
            inputs = inputs.to(device)
            labels = labels.to(device)

            # Clean existing gradients
            optimizer.zero_grad()

            # Forward pass - compute outputs on input data using the model
            outputs = model(inputs)

            # Compute loss
            loss = loss_criterion(outputs, labels)

            # Backpropagate the gradients
            loss.backward()

            # Update the parameters
            optimizer.step()

            # Compute the total loss for the batch and add it to train_loss
            train_loss += loss.item() * inputs.size(0)

            # Compute the accuracy
            ret, predictions = torch.max(outputs.data, 1)
            correct_counts = predictions.eq(labels.data.view_as(predictions))

            # Convert correct_counts to float and then compute the mean
            acc = torch.mean(correct_counts.type(torch.FloatTensor))

            # Compute total accuracy in the whole batch and add to train_acc
            train_acc += acc.item() * inputs.size(0)

            print("Batch number: {:03d}, Training: Loss: {:.4f}, Accuracy: {:.4f}".format(i, loss.item(), acc.item()))

        # Validation - No gradient tracking needed
        with torch.no_grad():

            # Set to evaluation mode
            model.eval()

            # Validation loop
            for j, (inputs, labels) in enumerate(valid_data_loader):
                inputs = inputs.to(device)
                labels = labels.to(device)

                # Forward pass - compute outputs on input data using the model
                outputs = model(inputs)

                # Compute loss
                loss = loss_criterion(outputs, labels)

                # Compute the total loss for the batch and add it to valid_loss
                valid_loss += loss.item() * inputs.size(0)

                # Calculate validation accuracy
                ret, predictions = torch.max(outputs.data, 1)
                correct_counts = predictions.eq(labels.data.view_as(predictions))

                # Convert correct_counts to float and then compute the mean
                acc = torch.mean(correct_counts.type(torch.FloatTensor))

                # Compute total accuracy in the whole batch and add to valid_acc
                valid_acc += acc.item() * inputs.size(0)

                print("Validation Batch number: {:03d}, Validation: Loss: {:.4f}, Accuracy: {:.4f}".format(j, loss.item(), acc.item()))

        if valid_loss < best_loss:
            best_loss = valid_loss
            best_epoch = epoch

        # Find average training loss and training accuracy
        avg_train_loss = train_loss / train_data_size
        avg_train_acc = train_acc / train_data_size

        # Find average training loss and training accuracy
        avg_valid_loss = valid_loss / valid_data_size
        avg_valid_acc = valid_acc / valid_data_size

        history.append([avg_train_loss, avg_valid_loss, avg_train_acc, avg_valid_acc])

        epoch_end = time.time()

        print(f"{'-'*90}\n"
              f"Epoch: {epoch :03d}\t Training:\t Loss - {avg_train_loss :.4f}\t Accuracy - {avg_train_acc * 100 :.4f}%\n"
              f"\t\t Validation:\t Loss - {avg_valid_loss:.4f}\t Accuracy - {avg_valid_acc * 100:.4f}%\t Time: {epoch_end - epoch_start:.4f}s")

        # Save if the model has best accuracy till now
        if best_epoch == epoch:
            torch.save(model, f'{PATH_TO_MODELS}/model_epoch_{epoch}.pth')

    return model, history, best_epoch


# Train
# -----
# TODO logs
# Train the model
trained_model, history, best_epoch = train_and_validate(convnxt, loss_func, optimizer, NUM_EPOCHS)


# Stats
# -----

# Save history
torch.save(history, f'{PATH_TO_MODELS}/stats_history.pth')

# Save Loss Curve
history = np.array(history)
plt.plot(history[:,0:2])
plt.legend(['Tr Loss', 'Val Loss'])
plt.xlabel('Epoch Number')
plt.ylabel('Loss')
plt.ylim(0,1)
plt.savefig(f'{PATH_TO_MODELS}/stats_loss_curve.png')
# plt.show()
plt.close()

# Save Accuracy Curve
plt.plot(history[:,2:4])
plt.legend(['Tr Accuracy', 'Val Accuracy'])
plt.xlabel('Epoch Number')
plt.ylabel('Accuracy')
plt.ylim(0,1)
plt.savefig(f'{PATH_TO_MODELS}/stats_accuracy_curve.png')
# plt.show()

# %%
