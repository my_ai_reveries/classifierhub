#%%
import torch

import os
from os.path import isfile, join
import re
import sys

# ==============================
#            Convert
# ==============================
sys.path.append('.')
PATH_TO_MODELS = './model/output/train'
PATH_TO_ONNX = './model/output/onnx'
EPOCH_NUM = 0
#%%
if(not os.path.exists(PATH_TO_ONNX)) :
    os.makedirs(PATH_TO_ONNX)
    print("created onnx folder")

model_names = [f for f in os.listdir(PATH_TO_MODELS) if isfile(join(PATH_TO_MODELS, f))]

p = re.compile(r'model_epoch_(\d+)\.pth')

for name in model_names :
    m = p.match(name)
    if (m) :
        tmp = int(m.group(1))   
        if(tmp > EPOCH_NUM ) :
            EPOCH_NUM = tmp

torch_model_weights_path = f"{PATH_TO_MODELS}/model_epoch_{EPOCH_NUM}.pth"
onnx_model_output_path = f"{PATH_TO_ONNX}/fine_tuned_classifier.onnx"

model = torch.load(torch_model_weights_path, map_location=torch.device('cpu'))
model.to('cpu')
model.eval()

print('Loaded')

# Input to the model
onnx_input = torch.randn(1, 3, 480, 640, requires_grad=True)  # (batch_size, channels, height, width)


# Export the ONNX model
torch.onnx.export(model,                               # model being run
                  onnx_input,                          # model input (or a tuple for multiple inputs)
                  onnx_model_output_path,              # where to save the model (can be a file or file-like object)
                  export_params=True,                  # store the trained parameter weights inside the model file
                  opset_version=12,                    # the ONNX version to export the model to
                  do_constant_folding=True,            # whether to execute constant folding for optimization
                  input_names=['input'],               # the model's input names
                  output_names=['output'],             # the model's output names
                  dynamic_axes={'input': [0, 2, 3],    # variable lenght axes
                                'output': [0, 2, 3]})  # image shape: [batch_size, channels, height, width]

print('Converted')


# %%
