# Classifier Hub

**ClassifierHub** is a Streamlit app designed to help you fine-tune custom image classification models with ease. Whether you're a developer, hobbyist, or business owner, this tool empowers you to create precise image recognition models using your own dataset and classes. Fine-tune, test, and retrieve your model for immediate use.

## Features

- **Custom Dataset Integration:** Bring your own dataset and define your classes for tailored image classification.

- **Fine-Tuning with ConvNext:** Utilize the powerful ConvNext model for fine-tuning, enabling high accuracy in image classification.

- **Real-time Testing:** Test your model with various images, making adjustments on the fly for optimal results.

- **Model Retrieval:** Retrieve your fine-tuned model for use in this app or download it to use in your projects.

## Getting Started

1. **Installation:**
   - Clone this repository.
   - Install the required dependencies using `pip install -r requirements.txt`.

2. **Running the App:**
   - Start the Streamlit app by running `streamlit run app.py`.
   - Access the app through your web browser (usually at `http://localhost:8501`).

3. **Customize and Fine-Tune:**
   - Upload your dataset and define your classes.
   - Fine-tune your model using ConvNext with the user-friendly interface.

4. **Real-time Testing:**
   - Test your model with various images to ensure accuracy.

5. **Model Retrieval:**
   - Retrieve your fine-tuned model for immediate use in the app or in your projects.

## Dependencies

- Python 3.x
- Streamlit
- ConvNext (or your choice of fine-tuning model)
- Other dependencies as specified in `requirements.txt`.

## Contributing

We welcome contributions from the community! Feel free to submit issues, suggest improvements, or open pull requests to enhance this project.

## License

This project is licensed under the [MIT License](LICENSE).

---

Experience the power of customization and precision in image classification with **ClassifierHub**. Your vision, your data, your classifications, made easy with Streamlit and ConvNext.

Note: In the future this project may be splited into 2 projects (app and backend)